# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the pantheon-files-searchs package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: pantheon-files-searchs\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-27 14:24+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/plugin.vala:36
msgid "Search"
msgstr ""

#: src/plugin.vala:37
msgid "You must enter at least one character."
msgstr ""

#: src/plugin.vala:38
msgid "No results found."
msgstr ""
