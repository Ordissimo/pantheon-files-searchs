/*
 * Copyright (C) 2020 Krifa75
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Tracker;
public class Files.Plugins.Searchs : Files.Plugins.Base {
    private Granite.Widgets.DynamicNotebook notebook;
    private Files.SidebarInterface? interface_sidebar;
    private Files.AbstractSlot slot_view;

    private Gtk.ButtonBox buttonsbox;
    private Gtk.ToggleButton selected_letter = null;

    private GLib.Cancellable cancellable = null; /* For tracker */
    private Gtk.Entry basic_bread;
    private Gtk.Button button_search;
    private int search_results = 0;
    private bool searchbar_init = false;

    private const string OPEN_FILE_LOCATION = N_("Open file location");
    private const string SEARCH_LABEL = N_("Search");
    private const string CANCEL_SEARCH_LABEL = N_("Cancel");
    private const string SEARCH_CHARACTERS = N_("You must enter at least one character.");
    private const string RESULTS_NOT_FOUND = N_("No results found.");

    private Gtk.Revealer revealer_search = null;
    private Gtk.Box white_box = null;
    private Gtk.Label results_not_found = null;

    private Gtk.Toolbar? pantheon_toolbar = null;
    private Gtk.ToolButton? open_file_location_button;

    private Settings zoom_settings;
    private IconSize current_zoom;

    private GLib.File file_scanner;
    private GLib.File file_searchs;

    private const string ALPHABETS_CSS_TEMPLATE = ".alphabetic-index * { font-size: %dpt; padding-top: 0; padding-bottom: 0;  min-height: 0; }";

    public Searchs () {
        /* Initiliaze gettext support */
        Intl.setlocale (LocaleCategory.ALL, "");

        buttonsbox = new Gtk.ButtonBox (Gtk.Orientation.VERTICAL) {
            layout_style=Gtk.ButtonBoxStyle.EXPAND
        };

        buttonsbox.get_style_context ().add_class ("alphabetic-index");
        buttonsbox.set_name ("alphabetic-index");
        create_alphabets ();

        revealer_search = new Gtk.Revealer () {
            reveal_child=false,
            halign=Gtk.Align.CENTER,
            valign=Gtk.Align.CENTER
        };

        Gtk.Label label_search = new Gtk.Label (_(SEARCH_CHARACTERS));
        label_search.get_style_context ().add_class (Granite.STYLE_CLASS_H1_LABEL);
        ((Gtk.Container)revealer_search).add (label_search);

        zoom_settings  = new Settings ("io.elementary.files.column-view");

        string scanner_directory = GLib.Path.build_path (Path.DIR_SEPARATOR_S, GLib.Environment.get_user_cache_dir (), "pantheon-files", "scanner-output");
        file_scanner = GLib.File.new_for_path (scanner_directory);

        string dummy_searchs = GLib.Path.build_path (Path.DIR_SEPARATOR_S, GLib.Environment.get_user_runtime_dir (), "pantheon-files", "dummy-searchs");
        file_searchs = GLib.File.new_for_path (dummy_searchs);
        if (!file_searchs.query_exists ()) {
            try {
                file_searchs.make_directory_with_parents ();
            } catch (Error e) {
                critical (e.message);
            }
        }
    }

    private async bool fetch_next_results (Sparql.Cursor cursor) {
        try {
            if (!yield cursor.next_async (cancellable))
                return false;

            GLib.File file = GLib.File.new_for_uri (cursor.get_string (0));
            Files.File gof = Files.File.@get(file);

            slot_view.directory.file_hash_add_file (gof);
            gof.ensure_query_info ();

            var thumbnailer = Files.Thumbnailer.get ();
            thumbnailer.queue_file (gof, null, false);

            slot_view.directory.file_added (gof);
            slot_view.set_all_selected (false); // The signal select the file;

            search_results++;
            if (search_results == 1) {
                remove_overlay ();
                if (pantheon_toolbar != null &&
                    !pantheon_toolbar.get_sensitive ())
                    pantheon_toolbar.set_sensitive (true);
            }
        } catch (Error e) {
            warning ("Could not fetch results : %s", e.message);
            update_label_search ();
            return false;
        }
        return yield fetch_next_results (cursor);
    }

    private async void find_with_tracker (string text) {
        Sparql.Connection connection;
        Sparql.Cursor cursor;

        /* Search case insensitive */
        string search_query = "SELECT DISTINCT ?url ?filename WHERE {\n" + 
                "   ?u a nfo:FileDataObject ; nie:url ?url ; nfo:fileName ?filename .\n" + 
                "   FILTER (CONTAINS (lcase(?filename),\"" + text + "\"))\n" + 
                "} ORDER BY ASC(?filename)";


        try {
            connection = Sparql.Connection.bus_new ("org.freedesktop.Tracker3.Miner.Files", null, null);
            if (connection == null)
                return;

            cursor = yield connection.query_async (search_query, cancellable);
            yield fetch_next_results (cursor);
        } catch (GLib.Error error) {
            critical ("Error connecting to Tracker: %s", error.message);
            update_label_search ();
        }
    }

    private Gtk.CssProvider remove_button_background () {
        Gtk.CssProvider provider = null;
        string css;

        provider = new Gtk.CssProvider ();

        css = "button.toggle:not(:checked) { background: none; border: none; box-shadow: none; min-height: 0; }";

        try {
            provider.load_from_data (css);
        } catch (Error e) {
            warning ("Failed to create provider for css %s : %s", css, e.message);
        }

        return provider;
    }

    private void create_alphabets () {
        var provider = remove_button_background ();
        for (char i = 'A'; i < 'Z' + 1; i++) {
            Gtk.ToggleButton button = new Gtk.ToggleButton.with_label (i.to_string()) {
                relief=Gtk.ReliefStyle.NONE
            };

            if (provider != null)
                button.get_style_context ().add_provider (provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

            button.enter_notify_event.connect ( () => {
                Gdk.Cursor pointer = new Gdk.Cursor.from_name (Gdk.Display.get_default(), "pointer");
                ((Gdk.Window)button.get_parent_window()).set_cursor (pointer);
                return true;
            });
            button.leave_notify_event.connect ( () => {
                ((Gdk.Window)button.get_parent_window()).set_cursor (null);
                return true;
            });

            button.button_press_event.connect ( () => {
                if (selected_letter != null && selected_letter.get_active ()) {
                    if (button == selected_letter)
                        return Gdk.EVENT_STOP;
                    selected_letter.set_active (false);
                }

                selected_letter = button;
                find_file_by_letter (button.get_label ());

                return Gdk.EVENT_PROPAGATE;
            });
            buttonsbox.add (button);
        }
    }

    private void add_overlay (bool results_not_found = false) {
        if (white_box == null) {
            white_box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            white_box.get_style_context ().add_class (Gtk.STYLE_CLASS_BACKGROUND);
            slot_view.overlay.add_overlay (white_box);
            // We reorder it, to avoid hiding revealer_search.
            slot_view.overlay.reorder_overlay (white_box, 0);
        }

        if (results_not_found) {
            this.results_not_found = new Gtk.Label (_(RESULTS_NOT_FOUND));
            this.results_not_found.get_style_context ().add_class (Granite.STYLE_CLASS_H1_LABEL);

            this.results_not_found.set_halign (Gtk.Align.CENTER);
            this.results_not_found.set_valign (Gtk.Align.CENTER);

            white_box.pack_start (this.results_not_found);
        }

        slot_view.overlay.show_all ();
    }

    private void remove_overlay () {
        if (white_box != null)
            slot_view.overlay.remove (white_box);
        white_box = null;
        results_not_found = null;
    }

    private void on_search_event () {
        string entry_text = basic_bread.get_text().strip ();
        if (entry_text.length < 2 || entry_text == "") {
            update_label_search ();
            cancellable.cancel ();
            cancellable = null;

            revealer_search.set_reveal_child (true);
            GLib.Timeout.add (2000, () => {
                revealer_search.set_reveal_child (false);
                return GLib.Source.REMOVE;
            });
            return;
        }

        if (slot_view.uri == file_searchs.get_uri ())
            slot_view.directory.need_reload (false);

        slot_view.user_path_change_request (file_searchs, true);

        update_toolbar (false);
        notebook.current.label = _(SEARCH_LABEL);

        search_results = 0;
        if (results_not_found != null) {
            white_box.remove (results_not_found);
            results_not_found = null;
        }

        add_overlay ();

        Files.ZoomLevel zoom_level = (Files.ZoomLevel)zoom_settings.get_enum ("zoom-level");
        current_zoom = zoom_level.to_icon_size ();

        find_with_tracker.begin (entry_text.down(), (obj, res) => {
            cancellable = null;

            slot_view.is_frozen = false;
            slot_view.grab_focus ();

            if (search_results == 0 && results_not_found == null) {
                add_overlay (true);
                if (pantheon_toolbar != null)
                    pantheon_toolbar.set_sensitive (false);
            }

            basic_bread.set_text ("");
            update_label_search ();
        });
    }

    private Gtk.Widget? get_widget_by_name (Gtk.Widget parent, string name) {
        if (parent.get_name () == name)
            return parent;
        else if (parent is Gtk.Container) {
            var children = ((Gtk.Container)parent).get_children ();
            foreach (var c in children) {
                var child = get_widget_by_name (c, name);
                if (child != null)
                    return child;
            }
        } else if (parent is Gtk.Bin) {
            var child = ((Gtk.Bin)parent).get_child ();
            return get_widget_by_name (child, name);
        }
        return null;
    }

    // Select_glib_files doesn't align the focused location which is necessary in our case
    // TODO: See if it's better to modify pantheon to avoid getting the view and loose genericity.
    private void find_file_by_letter (string letter) {
        GLib.return_if_fail (letter != null || letter.length != 1);
        Gtk.TreePath? path_file = null;

        Gtk.Widget view;
        Gtk.TreeModel? model_view = null;
        slot_view.grab_focus ();
        view = ((Gtk.Window)window).get_focus ();

        if (view is Gtk.TreeView) {
            model_view = ((Gtk.TreeView)view).get_model () as Gtk.TreeModel;
        } else if (view is Gtk.IconView)
            model_view = ((Gtk.IconView)view).get_model ();
        else {
            warning ("Could not determine which type of view is.");
            return;
        }

        model_view.@foreach ( (model, path, iter) => {
            string filename = null;
            model.get (iter, Files.ListModel.ColumnID.FILENAME, out filename, -1);

            if (filename == null || !filename[0].isalpha ())
                return false;

            if (filename.up ()[0] >= letter[0]) {
                path_file = path;
                return true;
            }
            return false;
        });

        if (path_file == null) {
            int len = model_view.iter_n_children (null);
            if (len > 0)
                len -= 1;

            path_file = new Gtk.TreePath.from_indices (len, -1);
        }

        if (view is Gtk.TreeView) {
            ((Gtk.TreeView)view).scroll_to_cell (path_file, null, true, 0.0f, 1.0f);
        } else if (view is Gtk.IconView) {
            ((Gtk.IconView)view).scroll_to_path (path_file, true, 0.0f, 1.0f);
        }
    }

    private void update_toolbar (bool set_visible) {
        if (pantheon_toolbar == null)
            return;

        /* TODO: Maybe find by name in case we modify toolbar.ui later */
        pantheon_toolbar.get_nth_item (0).set_visible (set_visible); /* Create */
        pantheon_toolbar.get_nth_item (3).set_visible (set_visible); /* Paste */
        pantheon_toolbar.get_nth_item (4).set_visible (set_visible); /* Rename */
        pantheon_toolbar.get_nth_item (5).set_visible (set_visible); /* Move */
        pantheon_toolbar.get_nth_item (8).set_visible (set_visible); /* Print */
        pantheon_toolbar.get_nth_item (9).set_visible (set_visible); /* Mail */
        pantheon_toolbar.get_nth_item (11).set_visible (set_visible); /* Open file location */
    }


    private void update_label_search () {
        if (button_search.label == _(SEARCH_LABEL)) {
            button_search.label = _(CANCEL_SEARCH_LABEL);
        } else if (button_search.label == _(CANCEL_SEARCH_LABEL)) {
            button_search.label = _(SEARCH_LABEL);
        }
    }

    private void search_event () {
        update_label_search ();

        if (cancellable == null) {
            cancellable = new GLib.Cancellable ();
            on_search_event ();
        } else {
            cancellable.cancel ();
            cancellable = null;
        }
    }

    public override void sidebar_loaded (Gtk.Widget widget) {
        interface_sidebar = widget as Files.SidebarInterface;
    }

    private void construct_search_bar () {
        Gtk.Box boxed_window = interface_sidebar.get_parent () as Gtk.Box;

        notebook = boxed_window.get_children ().last ().data as Granite.Widgets.DynamicNotebook;

        ((Gtk.Container)boxed_window).remove (notebook);

        Gtk.Image image_search = new Gtk.Image.from_icon_name ("edit-find-symbolic", Gtk.IconSize.BUTTON);
        button_search = new Gtk.Button.with_label (_(SEARCH_LABEL)) {
            image=image_search,
            always_show_image=true,
            image_position=Gtk.PositionType.LEFT
        };

        basic_bread = new Gtk.Entry ();
        basic_bread.set_placeholder_text (_(SEARCH_LABEL));

        Gtk.Box search_bar = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        search_bar.pack_start (basic_bread, true, true);
        search_bar.pack_start (button_search, false, false);

        button_search.clicked.connect ( (ev) => {
            search_event ();
        });

        basic_bread.key_press_event.connect (basic_bread_on_key_event);
        basic_bread.key_release_event.connect (basic_bread_on_key_event);
        basic_bread.insert_text.connect (basic_bread_on_insert_text);

        Gtk.Box hbox = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
        hbox.pack_start (buttonsbox, false, false);
        hbox.pack_start (notebook, true, true);

        Gtk.Box box_notebook = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
        box_notebook.add (search_bar);
        box_notebook.pack_start (hbox, true, true);

        box_notebook.key_press_event.connect (box_notebook_on_key_press);

        pantheon_toolbar = get_widget_by_name (window as Gtk.Widget, "toolbar") as Gtk.Toolbar;
        if (pantheon_toolbar != null) {
            open_file_location_button = pantheon_toolbar.get_nth_item (11) as Gtk.ToolButton;
            open_file_location_button.clicked.connect (on_click_open_location);

            // Update sensitive toolbar
            box_notebook.button_release_event.connect_after (box_notebook_on_button_release);
        }

        ((Gtk.Box)boxed_window).pack_start (box_notebook, true, true);
        box_notebook.show_all ();

        window.configure_event.connect (update_size_buttonsbox);
    }

    private bool basic_bread_on_key_event (Gdk.EventKey ev) {
        if (ev.keyval != Gdk.Key.KP_Enter && ev.keyval != Gdk.Key.Return)
            return Gdk.EVENT_PROPAGATE;

        if (ev.type == Gdk.EventType.KEY_PRESS)
            search_event ();
        else if (ev.type == Gdk.EventType.KEY_RELEASE)
            basic_bread.set_text ("");
        return Gdk.EVENT_PROPAGATE;
    }

    private void basic_bread_on_insert_text (string new_character, int new_text_length, ref int position) {
        if (position <= 0)
            return;
        // Block if multiple space successively
        if (basic_bread.get_chars (position - 1, -1) == " " && new_character == " ")
            GLib.Signal.stop_emission_by_name (basic_bread, "insert-text");
    }

    private bool box_notebook_on_key_press (Gdk.EventKey ev) {
        if (basic_bread.get_text () == "" && !basic_bread.is_focus) {
            string letter = ev.str;
            if (letter[0].isalnum ()) {
                basic_bread.set_text (letter);
                basic_bread.grab_focus_without_selecting ();
                basic_bread.set_position (-1);
            }
        }
        return Gdk.EVENT_STOP; // Stop entry from pantheon;
    }

    private bool box_notebook_on_button_release () {
        if (slot_view.get_root_uri () != file_searchs.get_uri ()) {
            open_file_location_button.set_visible (false);
        } else if (slot_view.uri == file_searchs.get_uri () && search_results == 0)
            pantheon_toolbar.set_sensitive (false);
        else if (!pantheon_toolbar.get_sensitive ())
            pantheon_toolbar.set_sensitive (true);
        return Gdk.EVENT_STOP;
    }

    private void on_click_open_location () {
        open_location ();
    }

    private string? get_root_from_gfile (GLib.File file) {
        const GLib.UserDirectory[] DIRECTORIES = {
            GLib.UserDirectory.DOCUMENTS,
            GLib.UserDirectory.DOWNLOAD,
            GLib.UserDirectory.MUSIC,
            GLib.UserDirectory.PUBLIC_SHARE,
            GLib.UserDirectory.PICTURES,
            GLib.UserDirectory.TEMPLATES,
            GLib.UserDirectory.VIDEOS
        };

        string file_path = file.get_path ();
        string root_uri = null;
        foreach (GLib.UserDirectory directory in DIRECTORIES) {
            unowned string? dir_s = GLib.Environment.get_user_special_dir (directory);
            if (file_path.has_prefix (dir_s)) {
                root_uri = GLib.Path.build_path (Path.DIR_SEPARATOR_S, dir_s);
                break;
            }
        }

        if (root_uri == null &&
            file_path.has_prefix (PF.UserUtils.get_real_user_home ())) {
            root_uri = GLib.Path.build_path (Path.DIR_SEPARATOR_S, PF.UserUtils.get_real_user_home ());
        }
        return root_uri;
    }

    private bool reload_on_idle () {
        slot_view.reload ();
        return GLib.Source.REMOVE;
    }

    // For unknown reasons, the view is not refreshed and
    // so we see only the root unless we reload the directory.
    // Also after reloading, the selected folder on each slot is not in the range.
    private void on_slot_active () {
        Files.AbstractSlot current = slot_view.get_current_slot ();
        if (current == null)
            return;
        GLib.Idle.add ( () => {
            unowned GLib.List<Files.File>? gofs;
            GLib.List<GLib.File> selected_files;

            gofs = current.get_selected_files ();
            if (gofs == null)
                return GLib.Source.REMOVE;

            selected_files = new GLib.List<GLib.File>();
            selected_files.prepend (gofs.data.location);

            current.select_glib_files (selected_files, gofs.data.location);
            Thread.usleep (10000);
            GLib.Idle.add (reload_on_idle);
            return GLib.Source.REMOVE;
        });
    }

    private GLib.File? file_to_select = null;
    private void on_done_loading_dir () {
        slot_view.active.disconnect (on_slot_active);
        // slot_view.directory.done_loading.disconnect (on_done_loading_dir);
        if (file_to_select == null)
            return;

        var l = new GLib.List<GLib.File>();
        l.prepend (file_to_select);
        slot_view.set_active_state (true);

        message ("on_done_loading_dir called.");

        GLib.Timeout.add (500, () => {
            slot_view.select_glib_files (l, file_to_select);
            return GLib.Source.REMOVE;
        });
    }

    private bool open_location () {
        unowned GLib.List<Files.File>? selected_gofs;
        GLib.File selected_file;
        GLib.File root_file;
        string root_path;

        selected_gofs = slot_view.get_selected_files ();
        if (selected_gofs != null) {
            if (selected_gofs.data.is_folder ())
                selected_file = selected_gofs.data.location;
            else {
                file_to_select = selected_gofs.data.location;
                selected_file = selected_gofs.data.directory;
            }
        } else
            selected_file = slot_view.location;

        root_path = get_root_from_gfile (selected_file);
        root_file = Files.FileUtils.get_file_for_path (root_path);

        slot_view.active.connect_after (on_slot_active);
        slot_view.user_path_change_request (root_file, true);

        GLib.Idle.add ( () => {
            slot_view.user_path_change_request (selected_file, false);
            slot_view.directory.done_loading.connect_after (on_done_loading_dir);
            return GLib.Source.REMOVE;
        });


        return Gdk.EVENT_STOP;
    }

    public override void context_menu (Gtk.Widget widget, GLib.List<Files.File> selected_files) {
        if (selected_files == null || slot_view.get_root_uri () != file_searchs.get_uri ())
            return;

        var menu = widget as Gtk.Menu;
        var menu_item = new Gtk.MenuItem.with_label (_(OPEN_FILE_LOCATION));
        menu_item.button_press_event.connect (open_location);
        add_menuitem (menu, new Gtk.SeparatorMenuItem ());
        add_menuitem (menu, menu_item);
    }

    private void add_menuitem (Gtk.Menu menu, Gtk.MenuItem menu_item) {
        menu.append (menu_item);
        menu_item.show ();
    }

    /*
     * Depending on the resolution of the screen, the buttonsbox may exceed.
    */
    private bool update_size_buttonsbox (Gdk.EventConfigure event) {
        unowned var screen = Gdk.Screen.get_default ();
        var new_size = (double) event.height / 1000.0 * 12.0;

        try {
            var win_provider = new Gtk.CssProvider ();
            string css_template = ALPHABETS_CSS_TEMPLATE.printf ((uint)new_size-1);
            win_provider.load_from_data (css_template, -1);

            Gtk.StyleContext.add_provider_for_screen (screen, win_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
        } catch (Error e) {
            warning ("Could not load css data for buttonsbox : %s", e.message);
        }
        return Gdk.EVENT_PROPAGATE;
    }

    private void on_selection_changed (GLib.List<Files.File> files) {
        if (slot_view.get_root_uri () != file_searchs.get_uri ()) {
            open_file_location_button.set_visible (false);
            return;
        }
        else if (files.length () > 1) {
            if (open_file_location_button.get_visible ())
                open_file_location_button.set_visible (false);
        } else if (files.length () == 1) {
            if (!open_file_location_button.get_visible ())
                open_file_location_button.set_visible (true);
        }
        return;
    }

    public override void directory_loaded (Gtk.ApplicationWindow window, Files.AbstractSlot view, Files.File directory) {
        slot_view = view;

        if (!searchbar_init) {
            construct_search_bar ();

            slot_view.overlay.add_overlay (revealer_search);
            slot_view.overlay.show ();
            revealer_search.show_all ();

            searchbar_init = true;
        }

        if (view.get_root_uri () == file_searchs.get_uri ()) {
            update_toolbar (false);
            notebook.current.label = _(SEARCH_LABEL);
            if (pantheon_toolbar != null)
                slot_view.selection_changed.connect_after (on_selection_changed);
        } else {
            if (notebook.current.label == _(SEARCH_LABEL))
                notebook.current.label = directory.basename;

            remove_overlay ();

            pantheon_toolbar.set_sensitive (true);
        }

        if (basic_bread.get_text () != "")
            basic_bread.set_text ("");

        if (selected_letter != null) {
            selected_letter.set_active (false);
            selected_letter = null;
        }

        if (slot_view.uri == file_scanner.get_uri ()) {
            buttonsbox.set_visible (false);
        } else if (!buttonsbox.get_visible ())
            buttonsbox.set_visible (true);
    }
}

public Files.Plugins.Base module_init () {
    return new Files.Plugins.Searchs ();
}
